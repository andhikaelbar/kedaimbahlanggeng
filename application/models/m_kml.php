<?php
 
class M_kml extends CI_Model{
    function tampil_data() {
        return $this->db->get('barang');
    }

    function input_data($data, $table) {
        $this->db->insert($table,$data);
    }

    public function getAll ($table) {
        return $this->db->get($table)->result();
    }

    function getwhere ($table,$kondisi) {
        return $this->db->get_where($table,$kondisi)->result();
    }    

    public function insert($table,$data){
        $this->db->insert($table, $data);
    }
 
    public function edit ($table, $data, $where){
        $this->db->update($table, $data, $where);
    }
 
    public function delete ($table, $where){
        $this->db->delete($table, $where);
    }

    function getsatu ($table, $kondisi) {
        return $this->db->get_where($table, $kondisi)->row();
    }

    function getsql ($sql) {
        return $this->db->query($sql)->result();
    }

}
