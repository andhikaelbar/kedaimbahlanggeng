<div class="container">
    
  <div class="row">
    <?php foreach ($products as $p) {?> 
      <div class="card" style="width: 18rem; margin: 15px; max-width: 700px">
          <div class="card-body">
              <img src="<?= base_url ()?>gambar/<?= $p->gambar ?>" class="card-img-top" alt="...">          
                <h5 class="title"><?= $p->nama ?></h5>
                    <p class="description"><?= $p->deskripsi ?></p>
                    <p class="price">Rp. <?= $p->harga ?></p>
          </div>
          <div class="card-footer">
              <a href="<?= (base_url()) ?>product/hapusProses?id_barang=<?= $p->id_barang ?>" class="btn btn-danger">Hapus</a>
          </div>
      </div>
    <?php } ?>
  </div>
</div>