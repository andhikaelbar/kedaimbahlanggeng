<!DOCTYPE html>
<html>
<head>
    <title>Menu</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo(base_url()) ?>/assets/dist/css/awal.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
    <table>
        <form method="POST" action="<?php echo base_url().'product/tambahProses'; ?>" enctype="multipart/form-data">
          <div class="mb-3">
                <label for="namaMakanan" class="form-label">Nama Makanan</label>
                <input type="text" name="nama" class="form-control" id="namaMakanan" aria-describedby="namaMakanan">
          </div>
          <div class="mb-3">
                <label for="deskripsiMakanan" class="form-label">Deskripsi</label>
                <input type="text" name="deskripsi" class="form-control" id="deskripsiMakanan">
          </div>
          <div class="mb-3">
                <label for="hargaMakanan" class="form-label">Harga</label>
                <input type="text" name="harga" class="form-control" id="hargaMakanan">
          </div>
          <div>
                <label for="gambarMakanan" class="form-label">Pilih Gambar Makanan: </label>
                <input type="file" id="img" name="gambar" accept="image/*">
          </div>
                <button type="submit" class="btn btn-success">Masuk</button>
        </form>
    </table>
</body>
</html>