<div class="container">

  <table class="table table-hover">
    <tr>
      <th>Nama</th>
      <th>Jumlah Pesan</th>
    </tr>
    <?php foreach ($user as $u): ?>
        <tr>
          <td>
            <?= $u->nama ?>
          </td>
          <td>
            <?= $u->pesan_berapa ?>
          </td>
          <td>
          <a href="<?= base_url('HomeAdmin/detail?id_user='.$u->id_user) ?>">
            <button class="btn btn-info">Detail</button>
          </a>
          </td>
        </tr>
      <?php endforeach ?>  
  </table>
</div>