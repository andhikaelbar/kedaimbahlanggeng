<div class="container">
  <div class="d-flex justify-content-around">
          <?php foreach ($pesanan as $p) {?> 
            <div class="card" style="width: 18rem;">
              <div class="card-body">
                <img src="<?= base_url ()?>gambar/<?= $p->gambar ?>" class="card-img-top" alt="...">

                  <h5 class="title"><?= $p->nama ?></h5>
                  <table class="table table-boderless">
                    <tr>
                      <td>Deskripsi</td>
                      <td><?= $p->deskripsi ?></td>
                    </tr>
                    <tr>
                      <td>Harga Satuan</td>
                      <td><?= $p->harga ?></td>
                    </tr>
                    <tr>
                      <td>Jumlah</td>
                      <td><?= $p->jumlah_pesan ?></td>
                    </tr>
                    <tr>
                      <td>Total Harga</td>
                      <td><?= $p->harga * $p->jumlah_pesan ?></td>
                    </tr>
                  </table>
                  <a href="<?= base_url() ?>homeAdmin/selesaiTransaksi?id_pesanan=<?= $p->id_pesanan ?>">
                    <button class="btn btn-primary">Selesai Transaksi</button>
                    </a>
                </div>
            </div>
              <?php } ?>
  </div>