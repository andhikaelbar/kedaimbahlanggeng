<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Halaman Masuk Admin</title>
	<!-- Google Font: Source Sans Pro -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	<!-- Font Awesome Icons -->
	<link rel="stylesheet" href="<?= base_url()?>/assets/plugins/fontawesome-free/css/all.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?= base_url()?>/assets/dist/css/adminlte.min.css">
	<!-- css tambahan -->
	<link rel="stylesheet" href="<?= base_url()?>/assets/css/style.css">

	<!-- REQUIRED SCRIPTS -->

	<!-- jQuery -->
	<script src="<?= base_url()?>/assets/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="<?= base_url()?>/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- AdminLTE App -->
	<script src="<?= base_url()?>/assets/dist/js/adminlte.min.js"></script>
	<!-- js tambahan -->
	<script type="text/javascript" src="<?= base_url() ?>assets/js/js.js"></script>
</head>
<body class="hold-transition register-page">

	<div class="register-box">
		<div class="register-logo">
			<b>Login Admin</b>
		</div>

		<div class="card">
			<div class="card-body register-card-body">
				<!-- <?php if($pesan == 'salah') {?>
					<center class="text-danger">Username atau password salah</center>
				<?php } ?> -->

				<form action="<?= base_url().'Login/loginprosesAdmin' ?>" method="post">

					<!-- username -->
					<b>Username:</b>
					<div class="input-group mb-3">
						<input type="text" autocomplete="off" name="username" id="username" class="form-control" placeholder="username" oninput="janganKosong('username')">
						<div class="input-group-append">
							<div class="input-group-text">
								<span class="fas fa-user"></span>
							</div>
						</div>
					</div>

					<!-- password -->
					<b>Password:</b>
					<div class="input-group mb-3">
						<input type="password" oninput="janganKosong('pass')" id="pass" name="password" class="form-control" placeholder="Password">
						<div class="input-group-append">
							<div class="input-group-text">
								<span class="fas fa-lock"></span>
							</div>
						</div>
					</div>

					<button type="submit" id="submit" class="btn btn-primary btn-block">Masuk</button>
				</form>
			</div>
		</div>
	</div>
</body>
</html>
