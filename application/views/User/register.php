<!DOCTYPE html>
<html>
<head>
	<title>Register</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo(base_url()) ?>/assets/dist/css/awal.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
</head>
<body>
	<div class="container">
        <form method="POST" action="<?= base_url()?>Daftar/daftarproses" class="form-horizontal" role="form">
            <h2>Register</h2>
            <div class="form-group">
                <label for="firstName" class="col-sm-3 control-label">Nama</label>
                <div class="col-sm-9">
                    <input autocomplete="off" name="nama" type="text" id="firstName" placeholder="Nama" class="form-control" autofocus>
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-3 control-label">No Whatsapp</label>
                <div class="col-sm-9">
                    <input autocomplete="off" type="number" id="email" placeholder="No Whatsapp" class="form-control" name= "no_wa">
                    <span class="help-block">Kami tidak akan menyebarkan no telepon mu .... </span>
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-3 control-label">Alamat</label>
                <div class="col-sm-9">
                    <input autocomplete="off" name="alamat" type="text" id="text" placeholder="Alamat" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-3 control-label">Password</label>
                <div class="col-sm-9">
                    <input name="password" type="password" id="password" placeholder="Password" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-3 control-label">Konfirmasi Password</label>
                <div class="col-sm-9">
                    <input name="password_konfirm" type="password" id="password" placeholder="Password" class="form-control">
                </div>
            </div>
                    <p>Kami menyarankan Anda untuk menyimpan data Anda sebelum registrasi .... </p>
                    <button type="submit" class="btn btn-primary btn-block">Register</button>
        </form>
    </div>
</body>
</html>
