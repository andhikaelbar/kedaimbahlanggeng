<div class="container">
  <div class="d-flex justify-content-around">
          <?php foreach ($pesanan as $p) {?> 
            <div class="card" style="width: 18rem;">
              <div class="card-body">
                <img src="<?= base_url ()?>gambar/<?= $p->gambar ?>" class="card-img-top" alt="...">

                  <h5 class="title"><?= $p->nama ?></h5>
                  <table class="table table-boderless">
                    <tr>
                      <td>Deskripsi</td>
                      <td><?= $p->deskripsi ?></td>
                    </tr>
                    <tr>
                      <td>Harga Satuan</td>
                      <td><?= $p->harga ?></td>
                    </tr>
                    <tr>
                      <td>Jumlah</td>
                      <td><?= $p->jumlah_pesan ?></td>
                    </tr>
                    <tr>
                      <td>Total Harga</td>
                      <td><?= $p->harga * $p->jumlah_pesan ?></td>
                    </tr>
                  </table>
                    
                </div>
            </div>
              <?php } ?>
  </div>