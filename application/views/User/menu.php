<script type="text/javascript">
  var banyakData;
  var total = [];
  

  window.onload = function () {
    banyakData = document.getElementById('totalBarang').value
    for (var i = 0; i < banyakData; i++) {
      total[i] = 0;
    }
  }

  function hitungSemua() {
    var totalhehe = 0;
    for (var i = 0; i < banyakData; i++) {
    totalhehe += total[i];
    }
    return totalhehe;
  }
</script>
<div class="container">
  <input type="hidden" id="totalBarang" value="<?= count($products) ?>">
    <form method="POST" action="<?= base_url()?>konfirmasi/checkoutproses">
  <div class="row">
          <?php $i = 0; foreach ($products as $p) {?> 
            <div class="card" style="width: 18rem; margin: 15px; max-width: 700px">
              <div class="card-body">
                <img src="<?= base_url ()?>gambar/<?= $p->gambar ?>" class="card-img-top" alt="...">

                  <h5 class="title"><?= $p->nama ?></h5>
                    <p class="description"><?= $p->deskripsi ?></p>
                    <p class="price"><?= $p->harga ?></p>
                </div>
              <div class="card-footer">
                <input type="number" name="JumlahPesanan<?= $p->id_barang?>" oninput="
                  total[<?= $i ?>] = <?= $p->harga ?> * document.getElementById('jumlahPesanan<?= $p->id_barang ?>').value
                  document.getElementById('totalharga').innerHTML = hitungSemua()
                " class="form-control" min="0" value="0" id="jumlahPesanan<?= $p->id_barang ?>">
              </div>
            </div>
              <?php $i++; } ?>
  </div>
  <br>
  <p style="position: absolute; right: 200px; bottom: 40px; font-size: 15px; font-weight: bold;">total bayar: Rp. <span id="totalharga">0</span></p>
  <input style="position: absolute; right: 50px; bottom: 50px;" class="btn btn-success float-right" type="submit" value="Checkout">
</div>


