<!DOCTYPE html>
<html>
<head>
	<title>Kedai Mbah Langgeng</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo(base_url()) ?>/assets/dist/css/awal.css">
</head>
<body>

	<div>
		<div class="logo">
			<img src="<?php echo(base_url()) ?>assets/img/app-logo.png">
		</div>

		<div class="kalimat-penutup">
			<p>Terima Kasih telah berbelanja</p>
		</div>
	</div>
		
	<div class="register">
		<a href="<?php echo (base_url()) ?>HomeUser">Ingin Pesan Lagi?</a>		
	</div>
</body>
</html>