<!DOCTYPE html>
<html>
<head>
	<title>Kedai Mbah Langgeng</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo(base_url()) ?>/assets/dist/css/awal.css">
</head>
<body>

	<div>
		<div class="logo">
			<img src="<?php echo(base_url()) ?>assets/img/app-logo.png">
		</div>

		<div class="masuk-button">
			<a href="<?php echo(base_url()) ?>Login" type="button" class="btn btn-success">Login</a>
		</div>
	</div>
		
	<div class="register">
		<p>Belum punya akun? <a href="<?php echo (base_url()) ?>Daftar">Buat disini!</a></p>		
	</div>
</body>
</html>