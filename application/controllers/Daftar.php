<?php  
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar extends CI_Controller {
	
	public function index() {
		$this->load->view('header');
		$this->load->view('user/register');
		$this->load->view('footer');
	}

	function daftarproses() {
		$nama = $_POST['nama'];
		$alamat = $_POST['alamat'];
		$no_wa = $_POST['no_wa'];
		$password = $_POST['password'];
		$password_konfirm = $_POST['password_konfirm'];
		if ($password != $password_konfirm || $nama == '' || $alamat == '' || $no_wa == '') {
			redirect(base_url('Daftar'));
		} 
		else { 
			$data = [
			'nama' => $nama,
			'alamat' => $alamat,
			'no_wa' => $no_wa,
			'password' => md5($password)
			];
			$this->m_kml->input_data($data,'user');
			redirect(base_url('login?pesan=berhasildaftar'));
		}
	}
}