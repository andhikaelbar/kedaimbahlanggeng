<?php  
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	public function index() {
		$this->load->view('header');
		$this->load->view('user/loginUser');
		$this->load->view('footer');
	}

	function loginproses() {
		$username = $_POST['username'];
		$password = md5($_POST['password']);

		$kondisi = ['nama' => $username, 'password' => $password];

		$jumlah = count($this->m_kml->getwhere('user',$kondisi));
		if ($jumlah == 0 ) {
			redirect(base_url('login'));
		}
		else {
			$user = $this->m_kml->getsatu('user',$kondisi);
			$_SESSION['id_user'] = $user->id_user;
			$_SESSION['status'] = 'loginuser';
			redirect(base_url('HomeUser'));  
		}
	}

	function loginAdmin() {
		$this->load->view('Admin/loginAdmin');
		$this->load->view('footer');
	}

	function loginprosesAdmin() {
		$username = $_POST['username'];
		$password = md5($_POST['password']);

		$kondisi = ['username' => $username, 'password' => $password];

		$jumlah = count($this->m_kml->getwhere('admin',$kondisi));
		if ($jumlah == 0 ) {
			redirect(base_url('Login/loginAdmin'));
		}
		else {
			$user = $this->m_kml->getsatu('admin',$kondisi);
			$_SESSION ['id_admin'] = $user->id;
			$_SESSION ['status'] = 'loginAdmin';
			redirect(base_url('HomeAdmin'));  
		}

		/*echo $username;
		echo $password;*/
	}

}