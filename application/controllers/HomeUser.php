<?php  
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeUser extends CI_Controller {

	function __construct() {
		parent::__construct();
		if (isset($_SESSION['status'])) {
			if ($_SESSION['status'] != 'loginuser') {
			redirect(base_url('login'));
			}
		}
	}
	
	public function index() {
		$data['products'] = $this->m_kml->getAll('barang');
		$data['user'] = $this->m_kml->getsatu('user',['id_user' => $_SESSION['id_user']]);
		$this->load->view('User/sidebarUser', $data);
		$this->load->view('User/menu');
		$this->load->view('footer');
	}

	function dipesan() {
		$data['pesanan'] = $this->m_kml->getsql('
			select b.nama as nama,
			b.harga as harga, 
			b.deskripsi as deskripsi,
			b.gambar as gambar,
			p.jumlah_pesanan as jumlah_pesan 
			 from pesanan p
			inner join barang b on b.id_barang = p.id_barang
			inner join user u on u.id_user = p.id_user
			where p.status = 0 and u.id_user = '.$_SESSION['id_user'].' 
			');
		$data['user'] = $this->m_kml->getSatu('user',['id_user' => $_SESSION['id_user']]);
		$this->load->view('User/sidebarUser', $data);
		$this->load->view('User/dipesan');
		$this->load->view('footer');
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
	}
}