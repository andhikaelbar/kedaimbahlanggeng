<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Awal extends CI_Controller {
	
	public function index() {
		$this->load->view('header');
		$this->load->view('Awal');
		$this->load->view('footer');
	}
}
