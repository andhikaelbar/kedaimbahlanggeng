<?php  
defined('BASEPATH') OR exit('No direct script access allowed');

class Konfirmasi extends CI_Controller {
	
	public function index() {
		$this->load->view('header');
		$this->load->view('User/checkout', ['totalharga' => $_GET['totalharga']]);
		$this->load->view('footer');
	}

	function checkoutproses() {
		$barang = $this->m_kml->getAll('barang');
		$totalharga = 0;
		foreach ($barang as $b) {
			$totalharga += $b->harga*$_POST['JumlahPesanan'.$b->id_barang]; 
			$data['id_barang'] = $b->id_barang;
			$data['id_user'] = $_SESSION['id_user'];
			$data['jumlah_pesanan'] = $_POST['JumlahPesanan'.$b->id_barang];
			if ($_POST['JumlahPesanan'.$b->id_barang] > 0) {
				$this->m_kml->input_data($data,'pesanan');
			}
			
		}
		// bikin 
		redirect(base_url('akhir'));
	}
}
