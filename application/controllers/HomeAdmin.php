<?php  
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeAdmin extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		if (isset($_SESSION['status'])) {
			if ($_SESSION['status'] != 'loginAdmin') {
			redirect(base_url('login/loginAdmin'));
			}
		}
	}

	public function index() {
		$data['products'] = $this->m_kml->getAll('barang');
		$data['admin'] = $this->m_kml->getsatu('admin',['id' => $_SESSION['id_admin']]);
		$this->load->view('Admin/sidebarAdmin', $data);
		$this->load->view('Admin/tambahMenu');
		$this->load->view('footer');
	}

	function Pesanan () {
		$data['user'] = $this->m_kml->getAll('user');
		foreach ($data['user'] as $u) {
			$u->pesan_berapa = count($this->m_kml->getwhere('pesanan',['id_user' => $u->id_user,'status'=>0]));
		}

		$data['admin'] = $this->m_kml->getsatu('admin',['id' => $_SESSION['id_admin']]);
		$this->load->view('Admin/sidebarAdmin', $data);
		$this->load->view('Admin/pesanan');
		$this->load->view('footer');
	}

	function detail () {
		$data['pesanan'] = $this->m_kml->getsql('
			select b.nama as nama,
			b.harga as harga,
			b.deskripsi as deskripsi,
			b.gambar as gambar,
			p.jumlah_pesanan as jumlah_pesan,
			p.id_pesanan as id_pesanan 
			 from pesanan p
			inner join barang b on b.id_barang = p.id_barang
			inner join user u on u.id_user = p.id_user
			where p.status = 0 and u.id_user = '.$_GET['id_user'].' 
			
		');

		$data['admin'] = $this->m_kml->getsatu('admin',['id' => $_SESSION['id_admin']]);
		$this->load->view('Admin/sidebarAdmin', $data);
		$this->load->view('Admin/detail_pesanan');
		$this->load->view('footer');
	}

	function selesaiTransaksi () {
		$id_pesanan = $_GET['id_pesanan'];
		$p = $this->m_kml->getsatu('pesanan', ['id_pesanan' => $id_pesanan]);
		$u = $this->m_kml->getsatu('user', ['id_user' => $p->id_user]);
		$this->m_kml->edit('pesanan',['status' => 1],['id_pesanan' => $id_pesanan]);

		redirect(base_url('HomeAdmin/detail?id_user='.$u->id_user));
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url().'welcome?pesan=logout');
	}
}
