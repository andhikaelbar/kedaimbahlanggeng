<?php  
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('m_kml');
		$this->load->library('form_validation');
	}
	
	public function index() {
		$data['products'] = $this->m_kml->getAll('barang');
		$data['admin'] = $this->m_kml->getsatu('admin',['id' => $_SESSION['id_admin']]);
		$this->load->view('Admin/sidebarAdmin', $data);
		$this->load->view('admin/insert_barang', $data);
		$this->load->view('footer');
	}

	public function tambah(){
    	$this->load->view('admin/insert_barang');
	}

	function tambahProses() {
		$nama = $this->input->post('nama');
		$deskripsi = $this->input->post('deskripsi');
		$harga = $this->input->post('harga');

		$config['upload_path'] = './gambar/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = 10000;
		$config['overwrite'] = FALSE;

		$this->load->library('upload', $config);
		if (! $this->upload->do_upload('gambar')) {
			echo $this->upload->display_errors();
		} else {
			$data_input = [
				'nama' => $_POST["nama"],
				'deskripsi' => $deskripsi,
				'harga' => $harga,
				'gambar' => $this->upload->file_name,
			];
			$this->m_kml->insert('barang', $data_input);
			redirect(base_url('HomeAdmin'));			
		}
	}

	public function hapusProses() {
		$barang = $this->m_kml->getsatu('barang', ['id_barang' => $_GET['id_barang']]);

		unlink('./gambar/'.$barang->gambar);
		$this->m_kml->delete('barang', ['id_barang' => $_GET['id_barang']]);
		redirect(base_url('HomeAdmin'));
	}

}