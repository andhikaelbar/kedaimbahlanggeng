-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 27, 2021 at 07:51 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kedai_mbah_langgeng`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` text NOT NULL,
  `nama` text NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `nama`, `password`) VALUES
(1, 'admin', 'Andhika Elbar', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_barang` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `harga` int(10) NOT NULL,
  `gambar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `nama`, `deskripsi`, `harga`, `gambar`) VALUES
(21, 'Seblak', 'Makanan pedas berkuah khas Jawa Barat', 7000, 'chefjunaa__92436611_162683054941825_3024261362158520237_n.jpg'),
(22, 'Takoyaki ', 'Jajanan tradisional asli Jepang', 5000, 'takoyaki.jpg'),
(24, 'Ramen', 'Makanan berkuah khas Jepang. ', 12000, '3461155501.png'),
(29, 'Cilor', 'Makanan yang terbuat dari aci yang dibalut telur.', 5000, 'photo.jpg'),
(30, 'Sostel', 'Sosis yg digoreng dibalut dengan telur.', 2000, 'IMG_20190125_203610.jpg'),
(31, 'Mie Ayam', 'Mie yang dibumbui dengan daging ayam', 8000, 'images_mie_Mie_ayam_14-mie-ayam-kampung-1200x798.jpg'),
(32, 'Pangsit Goreng', 'Makanan berupa daging cincang yg dibungkus lembaran tepung terigu. Digoreng dengan bumbu kari.', 10000, '5f2ea0165681a.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `pesanan`
--

CREATE TABLE `pesanan` (
  `id_pesanan` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `jumlah_pesanan` int(11) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pesanan`
--

INSERT INTO `pesanan` (`id_pesanan`, `id_barang`, `id_user`, `jumlah_pesanan`, `tanggal`, `status`) VALUES
(13, 21, 2, 7, '2021-10-15 00:00:00', 1),
(14, 21, 2, 3, '2021-10-16 09:28:09', 1),
(15, 21, 2, 1, '2021-10-16 09:29:12', 1),
(16, 22, 2, 1, '2021-10-16 09:29:12', 1),
(17, 21, 2, 1, '2021-10-16 09:37:07', 0),
(18, 22, 2, 2, '2021-10-16 09:37:07', 0),
(19, 22, 2, 2, '2021-10-22 10:37:53', 0),
(20, 22, 2, 2, '2021-10-22 10:37:54', 0),
(21, 21, 2, 2, '2021-10-24 21:21:44', 1),
(22, 21, 3, 2, '2021-10-24 21:35:41', 1),
(23, 22, 3, 1, '2021-10-24 21:36:39', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama` text NOT NULL,
  `password` varchar(100) NOT NULL,
  `no_wa` varchar(20) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama`, `password`, `no_wa`, `alamat`) VALUES
(1, 'asd', 'c4ca4238a0b923820dcc', '0888888888', 'mars'),
(2, 'a', '0cc175b9c0f1b6a831c399e269772661', '099999999', 'mars'),
(3, 'Udin', 'ee11cbb19052e40b07aac0ca060c23ee', '081212121212', 'Uranus');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `pesanan`
--
ALTER TABLE `pesanan`
  ADD PRIMARY KEY (`id_pesanan`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `pesanan`
--
ALTER TABLE `pesanan`
  MODIFY `id_pesanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
